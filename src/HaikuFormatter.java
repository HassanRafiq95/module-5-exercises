public class HaikuFormatter
{
    public static void main(String[] args) {
        haikuPrintMethod();
    }

    public static void haikuPrintMethod() {

        String[][] strings = {
                {"An", "Old", "Silent", "Pond..."},
                {"A", "frog", "jumps", "into", "the", "pond,"},
                {"splash!", "Silence", "again."}
        };

        for (String[] x : strings)
        {
            for(String y : x)
            {
                System.out.print(y + " ");
            }
            System.out.println();
        }
    }
}