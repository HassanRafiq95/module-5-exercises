import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Array;

public class BookManager {

    public static void main(String[] args) {
       //Book[] bookArray = new Book[2];
       //bookArray[0] = new Book("dsfdsfdsf","fdsfdsf","fsfdsfds");
        //bookArray[1] = new Book("dsfdsfdsf","fdsfdsf","fsfdsfds");
        //saveBookArray(bookArray);
        BookManager bookManager = new BookManager();
        saveBookArray(bookManager.loadArrayMethod());
    }


    public Book[] loadArrayMethod() {

        Book[] allbook = new Book[6];

        try {

            String csvFile = "data/books.csv";

            FileReader fileReader = new FileReader(csvFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);


            String nextLine = bufferedReader.readLine();

            for (int i = 0; i < allbook.length; i++) {
                nextLine = bufferedReader.readLine();

                if (nextLine != null) {
                    String[] strings = nextLine.split(",");

                    String title = strings[0];
                    String author = strings[1];
                    String date = strings[2];

                    allbook[i] = new Book(title, author, date);
                }
            }
            // Create 3 Book Objects
            allbook[3] = new Book("Lord Of The Rings: Return of The King", "J.R.R Tolkein", "1955");
            allbook[4] = new Book("Lord Of The Rings: The Two Towers", "J.R.R Tolkein", "1954");
            allbook[5] = new Book("Lord Of The Rings: The Fellowship of the Ring", "J.R.R Tolkein", "1954");

//            System.out.println("--------------------");
//
//            for (int i = 0; i < allbook.length; i++) {
//                System.out.println(allbook[i].getAuthor());
//                System.out.println(allbook[i].getTitle());
//                System.out.println(allbook[i].getDate());
//                System.out.println("--------------------");
//            }
        } catch (Exception e)         // Catch ANY Exception
        {
            System.out.println(e.getMessage());     // Print the Exception message
            System.out.println(e.getStackTrace());  // Print the Stack Trace
            System.exit(1);                         // (Optionally) Exit (with code 1 to indicate an error has occurred)
        }
        return allbook;
    }


    public static void printArray(Book[] books) {
        System.out.println("--------------------");

        for (Book book : books) {
            System.out.println(book.getAuthor());
            System.out.println(book.getTitle());
            System.out.println(book.getDate());
            System.out.println("--------------------");
        }
    }


    public static void saveBookArray(Book[] newBooks) {
        // sample books to save in out CSV File


        try {
            FileWriter csvWriter = new FileWriter("data/save_AllBooks.csv");

            // create "header" row first
            csvWriter.append("Title");
            csvWriter.append(",");               // comma separators
            csvWriter.append("Author");
            csvWriter.append(",");
            csvWriter.append("Date");
            csvWriter.append("\n");             // new line character

            // create a new line in the file for each Book

            for (int i = 0; i < newBooks.length; i++) {
                Book newBook = newBooks[i];

                csvWriter.append(newBook.getTitle());
                csvWriter.append(",");
                csvWriter.append(newBook.getAuthor());
                csvWriter.append(",");
                csvWriter.append(newBook.getDate());
                csvWriter.append("\n");
            }

            csvWriter.flush();
            csvWriter.close();


            // remember to flush and close when finished
        } catch (Exception e)         // Catch ANY Exception
        {
            System.out.println(e.getMessage());     // Print the Exception message
            System.out.println(e.getStackTrace());  // Print the Stack Trace
            System.exit(1);                         // (Optionally) Exit (with code 1 to indicate an error has occurred)
        }
    }
}
